## Atmosphere ##

* Serene (not all serene of course)
* There's living village, abandoned/crumbling settlement and lone shacks on the mountainside
* History, much like one in the _Dwarf Fortress_, decaying of the settlement is simulated by the very method, but 1-2 will be forced. The amount is dependent on the size of the map
* Reference: _Legend of Zelda: Breath of the Wild Trailer_ (the trailer came much after this game but the video will help you to get a grip)

## Colour overlay ##

Colour overlay is set to 6 500 K as untouched. The value must be

* Increased when:
    - Overcast (~ 7 000 K)
    - Freezing area (~ 7 500 K)
    
* Decreased when:
    - Tropical area (~ 5 500 K)
    
    
## Sunlight ##

Sunlight of the midday must have colour temperature of 5 700 K.


## Weather effects ##

* Wind
    - Blows away sands/snows
    - Tilts raindrops/snowfall
    
* Precipitation
    - Rain, snow