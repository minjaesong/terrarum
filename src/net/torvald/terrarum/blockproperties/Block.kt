package net.torvald.terrarum.blockproperties

/**
 * Created by minjaesong on 2016-02-21.
 */
object Block {

    const val AIR = 0 // hard coded; this is the standard

    const val STONE = 16
    const val STONE_QUARRIED = 17
    const val STONE_TILE_WHITE = 18
    const val STONE_BRICKS = 19

    const val DIRT = 32
    const val GRASS = 33
    const val GRASSWALL = 34

    const val PLANK_NORMAL = 48
    const val PLANK_EBONY = 49
    const val PLANK_BIRCH = 50
    const val PLANK_BLOODROSE = 51

    const val TRUNK_NORMAL = 64
    const val TRUNK_EBONY = 65
    const val TRUNK_BIRCH = 66
    const val TRUNK_BLOODROSE = 67

    const val SAND = 80
    const val SAND_WHITE = 81
    const val SAND_RED = 82
    const val SAND_DESERT = 83
    const val SAND_BLACK = 84
    const val SAND_GREEN = 85

    const val GRAVEL = 96
    const val GRAVEL_GREY = 97

    const val ORE_COPPER = 112
    const val ORE_IRON = 113
    const val ORE_GOLD = 114
    const val ORE_SILVER = 115
    const val ORE_ILMENITE = 116
    const val ORE_AURICHALCUM = 117

    const val RAW_RUBY = 128
    const val RAW_EMERALD = 129
    const val RAW_SAPPHIRE = 130
    const val RAW_TOPAZ = 131
    const val RAW_DIAMOND = 132
    const val RAW_AMETHYST = 133

    const val SNOW = 144
    const val ICE_FRAGILE = 145
    const val ICE_NATURAL = 146
    const val ICE_MAGICAL = 147

    const val GLASS_CRUDE = 148
    const val GLASS_CLEAN = 149

    const val PLATFORM_STONE = 160
    const val PLATFORM_WOODEN = 161
    const val PLATFORM_EBONY = 162
    const val PLATFORM_BIRCH = 163
    const val PLATFORM_BLOODROSE = 164

    const val TORCH = 176
    const val TORCH_FROST = 177

    const val TORCH_OFF = 192
    const val TORCH_FROST_OFF = 193

    const val ILLUMINATOR_WHITE = 208
    const val ILLUMINATOR_YELLOW = 209
    const val ILLUMINATOR_ORANGE = 210
    const val ILLUMINATOR_RED = 211
    const val ILLUMINATOR_FUCHSIA = 212
    const val ILLUMINATOR_PURPLE = 213
    const val ILLUMINATOR_BLUE = 214
    const val ILLUMINATOR_CYAN = 215
    const val ILLUMINATOR_GREEN = 216
    const val ILLUMINATOR_GREEN_DARK = 217
    const val ILLUMINATOR_BROWN = 218
    const val ILLUMINATOR_TAN = 219
    const val ILLUMINATOR_GREY_LIGHT = 220
    const val ILLUMINATOR_GREY_MED = 221
    const val ILLUMINATOR_GREY_DARK = 222
    const val ILLUMINATOR_BLACK = 223

    const val ILLUMINATOR_WHITE_OFF = 224
    const val ILLUMINATOR_YELLOW_OFF = 225
    const val ILLUMINATOR_ORANGE_OFF = 226
    const val ILLUMINATOR_RED_OFF = 227
    const val ILLUMINATOR_FUCHSIA_OFF = 228
    const val ILLUMINATOR_PURPLE_OFF = 229
    const val ILLUMINATOR_BLUE_OFF = 230
    const val ILLUMINATOR_CYAN_OFF = 231
    const val ILLUMINATOR_GREEN_OFF = 232
    const val ILLUMINATOR_GREEN_DARK_OFF = 233
    const val ILLUMINATOR_BROWN_OFF = 234
    const val ILLUMINATOR_TAN_OFF = 235
    const val ILLUMINATOR_GREY_LIGHT_OFF = 236
    const val ILLUMINATOR_GREY_MED_OFF = 237
    const val ILLUMINATOR_GREY_DARK_OFF = 238
    const val ILLUMINATOR_BLACK_OFF = 239

    const val SANDSTONE = 240
    const val SANDSTONE_WHITE = 241
    const val SANDSTONE_RED = 242
    const val SANDSTONE_DESERT = 243
    const val SANDSTONE_BLACK = 244
    const val SANDSTONE_GREEN = 245

    const val LANTERN = 256
    const val SUNSTONE = 257
    const val DAYLIGHT_CAPACITOR = 258


    const val ACTORBLOCK_NO_COLLISION = 4091
    const val ACTORBLOCK_FULL_COLLISION = 4092
    const val ACTORBLOCK_ALLOW_MOVE_DOWN = 4093
    const val ACTORBLOCK_NO_PASS_RIGHT = 4094
    const val ACTORBLOCK_NO_PASS_LEFT = 4095


    const val LAVA = 4094
    const val WATER = 4095

    const val NULL = -1

    val actorblocks = listOf(
            ACTORBLOCK_NO_COLLISION,
            ACTORBLOCK_FULL_COLLISION,
            ACTORBLOCK_ALLOW_MOVE_DOWN,
            ACTORBLOCK_NO_PASS_RIGHT,
            ACTORBLOCK_NO_PASS_LEFT
    )
}
