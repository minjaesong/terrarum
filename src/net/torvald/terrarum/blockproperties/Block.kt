package net.torvald.terrarum.blockproperties

/**
 * Created by minjaesong on 2016-02-21.
 */
object Block {

    val AIR = 0 // hard coded; this is the standard

    val STONE = 16
    val STONE_QUARRIED = 17
    val STONE_TILE_WHITE = 18
    val STONE_BRICKS = 19

    val DIRT = 32
    val GRASS = 33
    val GRASSWALL = 34

    val PLANK_NORMAL = 48
    val PLANK_EBONY = 49
    val PLANK_BIRCH = 50
    val PLANK_BLOODROSE = 51

    val TRUNK_NORMAL = 64
    val TRUNK_EBONY = 65
    val TRUNK_BIRCH = 66
    val TRUNK_BLOODROSE = 67

    val SAND = 80
    val SAND_WHITE = 81
    val SAND_RED = 82
    val SAND_DESERT = 83
    val SAND_BLACK = 84
    val SAND_GREEN = 85

    val GRAVEL = 96
    val GRAVEL_GREY = 97

    val ORE_COPPER = 112
    val ORE_IRON = 113
    val ORE_GOLD = 114
    val ORE_SILVER = 115
    val ORE_ILMENITE = 116
    val ORE_AURICHALCUM = 117

    val RAW_RUBY = 128
    val RAW_EMERALD = 129
    val RAW_SAPPHIRE = 130
    val RAW_TOPAZ = 131
    val RAW_DIAMOND = 132
    val RAW_AMETHYST = 133

    val SNOW = 144
    val ICE_FRAGILE = 145
    val ICE_NATURAL = 146
    val ICE_MAGICAL = 147

    val GLASS_CRUDE = 148
    val GLASS_CLEAN = 149

    val PLATFORM_STONE = 160
    val PLATFORM_WOODEN = 161
    val PLATFORM_EBONY = 162
    val PLATFORM_BIRCH = 163
    val PLATFORM_BLOODROSE = 164

    val TORCH = 176
    val TORCH_FROST = 177

    val TORCH_OFF = 192
    val TORCH_FROST_OFF = 193

    val ILLUMINATOR_WHITE = 208
    val ILLUMINATOR_YELLOW = 209
    val ILLUMINATOR_ORANGE = 210
    val ILLUMINATOR_RED = 211
    val ILLUMINATOR_FUCHSIA = 212
    val ILLUMINATOR_PURPLE = 213
    val ILLUMINATOR_BLUE = 214
    val ILLUMINATOR_CYAN = 215
    val ILLUMINATOR_GREEN = 216
    val ILLUMINATOR_GREEN_DARK = 217
    val ILLUMINATOR_BROWN = 218
    val ILLUMINATOR_TAN = 219
    val ILLUMINATOR_GREY_LIGHT = 220
    val ILLUMINATOR_GREY_MED = 221
    val ILLUMINATOR_GREY_DARK = 222
    val ILLUMINATOR_BLACK = 223

    val ILLUMINATOR_WHITE_OFF = 224
    val ILLUMINATOR_YELLOW_OFF = 225
    val ILLUMINATOR_ORANGE_OFF = 226
    val ILLUMINATOR_RED_OFF = 227
    val ILLUMINATOR_FUCHSIA_OFF = 228
    val ILLUMINATOR_PURPLE_OFF = 229
    val ILLUMINATOR_BLUE_OFF = 230
    val ILLUMINATOR_CYAN_OFF = 231
    val ILLUMINATOR_GREEN_OFF = 232
    val ILLUMINATOR_GREEN_DARK_OFF = 233
    val ILLUMINATOR_BROWN_OFF = 234
    val ILLUMINATOR_TAN_OFF = 235
    val ILLUMINATOR_GREY_LIGHT_OFF = 236
    val ILLUMINATOR_GREY_MED_OFF = 237
    val ILLUMINATOR_GREY_DARK_OFF = 238
    val ILLUMINATOR_BLACK_OFF = 239

    val SANDSTONE = 240
    val SANDSTONE_WHITE = 241
    val SANDSTONE_RED = 242
    val SANDSTONE_DESERT = 243
    val SANDSTONE_BLACK = 244
    val SANDSTONE_GREEN = 245

    val LANTERN = 256
    val SUNSTONE = 257
    val DAYLIGHT_CAPACITOR = 258

    val WATER_1 =  4080
    val WATER_2 =  4081
    val WATER_3 =  4082
    val WATER_4 =  4083
    val WATER_5 =  4084
    val WATER_6 =  4085
    val WATER_7 =  4086
    val WATER_8 =  4087
    val WATER_9 =  4088
    val WATER_10 = 4089
    val WATER_11 = 4090
    val WATER_12 = 4091
    val WATER_13 = 4092
    val WATER_14 = 4093
    val WATER_15 = 4094
    val WATER =    4095

    val LAVA_1 =  4064
    val LAVA_2 =  4065
    val LAVA_3 =  4066
    val LAVA_4 =  4067
    val LAVA_5 =  4068
    val LAVA_6 =  4069
    val LAVA_7 =  4070
    val LAVA_8 =  4071
    val LAVA_9 =  4072
    val LAVA_10 = 4073
    val LAVA_11 = 4074
    val LAVA_12 = 4075
    val LAVA_13 = 4076
    val LAVA_14 = 4077
    val LAVA_15 = 4078
    val LAVA =    4079

    val NULL = -1
}
