package net.torvald.terrarum.gameworld

/**
 * Created by minjaesong on 2016-08-06.
 */
object FluidCodex {
    const val FLUID_LAVA = 0xFE.toByte()
    const val FLUID_WATER = 0xFF.toByte()
}