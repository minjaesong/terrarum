## Minimum requirements ##

* Processor with 2.4 GHz speed
* GPU that can support OpenGL 2.1, is capable of 4K texture
* 6 GB of RAM
* 4 GB of free disk space
* Windows Vista/Mac OS X Lion or higher (Mac OS X Snow Leopard is incompatible with a shader the game uses)
* PC: Java 8, Up-to-date graphics driver

## Recommended requirements ##

* Processor with 3.0 GHz speed, 4 threads available
* GPU that can support OpenGL 2.1, is capable of 4K texture
* 8 GB of RAM
* 8 GB of free disk space
* Windows Vista/Mac OS X Lion or higher
* PC: Java 8, Up-to-date graphics driver

## Tested environment ##

(to devs: please extend this list with your test results!)

* MacBookPro9,2 (MacBook Pro 13 inch mid-2012)
* Intel 6700K, nVidia GTX970, Windows 10

